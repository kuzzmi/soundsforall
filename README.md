# README #

This is a sample web application made to cover requirements for JavaScript and Angular.js.

An application is a SoundCloud wrapper and can grant access to music anonymously (without account).
It was made with simple responsive design and can work on some portable devices.

Angular.js is a JavaScript library so main goals were for Angular.js:

* custom directives: elements and attributes
* custom filters
* templates
* using `$http` object to cover requirement for AJAX

Application features: 

* HTML5 audio API
* work with SoundCloud passing CORS
* anonymous access to SoundCloud
* displaying genres and music
* continuous music playing within each category
* infinite playlist loading on scroll
* simple responsiveness

To pass CORS `proxy.php` was made. It adds necessary headers and just retranslates needed requests and responses between application and SoundCloud.

To see application in action, please, look for http://kuzzmi.com/player.

To deploy application locally just clone repository to your local web-server folder.

Nothing is required for application launch.