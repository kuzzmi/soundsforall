(function() {

    /**
     * genres Module
     *
     * Provides definition of 'genres' behaviour
     */
    angular.module('genres', []).directive('genres',
        function(SoundsService) {
            return {
                restrict: 'E',
                templateUrl: 'html/genres.html',

                controller: function($scope) {
                    var me = this;
                    this.selected = 0;
                    this.data = null;

                    var initSoundsLoad = function(event, args) {
                        SoundsService.fetch($scope.genres[me.selected].uri)
                            .success(function(data) {
                                $scope.sounds = data.tracks;
                                SoundsService.nextUrl = data.next_href;
                                SoundsService.loadMore().success(function(next) {
                                    $scope.sounds = $scope.sounds.concat(next.tracks);

                                    SoundsService.nextUrl = next.next_href;
                                    $scope.$emit('soundsLoaded');
                                });
                            });
                    };

                    $scope.$on('connected', initSoundsLoad);

                    this.isSelected = function(index) {
                        return this.selected === index;
                    };

                    this.select = function(index, genre) {
                        this.selected = index;
                        initSoundsLoad();
                    };
                },

                controllerAs: 'genresCtrl'
            };
        });

})();