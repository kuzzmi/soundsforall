(function() {

    var app = angular.module('soundcloud', ['config']);

    var SoundCloud = function($scope, $http, BasicConfig) {
        var me = this;

        // SoundCloud application initialize
        SC.initialize({
            client_id: 'be8d16e3b2e1916e3fbe8291b402912e',
            redirect_uri: BasicConfig.baseUrl + 'callback.html'
        });

        var prepareData = function(data) {
            var corrected = data.join('|').replace(/\+/g, ' ').replace(/%26/g, '&').split('|');
            var result = [];

            for (var i = 1; i < data.length; i++) {
                result.push({
                    id: i,
                    uri: data[i],
                    name: corrected[i]
                });
            }

            return result;
        };

        me.authorized = false;

        var proceed = function() {
            me.authorized = true;

            $http.post(BasicConfig.baseUrl + 'proxy.php', {
                url: 'https://api-v2.soundcloud.com/explore/categories'
            }).success(function(data) {
                $scope.genres = prepareData(data.music);
                $scope.$emit('connected');
                $scope.ready = true;
            });
        };

        this.connect = function() {
            SC.connect(function() {
                proceed();
            });
        };

        this.connectAnon = function() {
            proceed();
        };
    };

    app.directive('soundcloudAuthorize', ['$http',
        function($http) {
            return {
                restrict: 'E',
                templateUrl: 'html/soundcloud.html',
                controller: SoundCloud,
                controllerAs: 'soundcloud'
            }
        }
    ]);

})();