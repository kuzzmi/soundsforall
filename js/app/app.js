(function() {

    // Defining main module
    var app = angular.module('player', ['config', 'soundcloud', 'sounds', 'genres']);

    app.service('SoundsService', ['$http', 'BasicConfig',
        function($http, BasicConfig) {
            this.fetch = function(genre) {
                return $http.post(BasicConfig.baseUrl + 'proxy.php', {
                    url: 'https://api-v2.soundcloud.com/explore/' + genre
                });
            };
            this.loadMore = function(nextUrl) {
                if (nextUrl)
                    this.nextUrl = nextUrl;
                return $http.post(BasicConfig.baseUrl + 'proxy.php', {
                    url: this.nextUrl
                });
            };
            this.nextUrl = null;
        }
    ]);

    app.directive('whenScrolled', function() {
        return function(scope, elm, attr) {
            var raw = elm[0];

            elm.bind('scroll', function() {
                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                    scope.$apply(attr.whenScrolled);
                }
            });
        };
    });

    app.directive('stopEvent', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                element.bind('click', function(e) {
                    e.stopPropagation();
                });
            }
        };
    });

})();