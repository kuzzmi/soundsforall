(function() {

    /**
     * sounds Module
     *
     * Provides playlist behaviour
     */
    var app = angular.module('sounds', []);

    app.filter('duration', function() {
        return function(duration) {
            var zeroPad = function(num, count) {
                var numZeropad = num + '';
                while (numZeropad.length < count) {
                    numZeropad = '0' + numZeropad;
                }
                return numZeropad;
            };

            var secDuration = ~~ (duration / 1000);
            var mins = ~~ (secDuration / 60);
            var seconds = secDuration - 60 * mins;
            return zeroPad(mins, 2) + ':' + zeroPad(seconds, 2);
        };
    });

    app.directive('currentTime', function($interval, $filter) {
        return function(scope, element, attrs) {
            var me = this,
                time = 0,
                stopTime,
                audio = document.getElementById('audio');

            audio.ontimeupdate = function() {
                var ms = ~~audio.currentTime * 1000;
                me.time = ms;
            };

            function updateTime() {
                element.text($filter('duration')(me.time) + ' /');
            }

            stopTime = $interval(updateTime, 1000);

            element.on('$destroy', function() {
                $interval.cancel(stopTime);
            });
        }
    });

    app.directive('sounds', function(SoundsService) {
        return {
            restrict: 'E',
            templateUrl: 'html/sounds.html',

            controller: function($scope) {
                var me = this;
                this.current = null;
                this.isPaused = false;

                var audio = document.getElementById('audio');

                var setScrolling = function() {
                    setTimeout(function() {
                        $('.nano').nanoScroller();
                    }, 10);
                };

                $scope.$on('soundsLoaded', setScrolling);

                audio.onended = function() {
                    me.next();
                };

                this.play = function(sound) {
                    if (me.isCurrent(sound.id)) {
                        if (!me.isPaused)
                            return me.pause();
                        else
                            return me.resume();
                    }

                    me.isPaused = false;
                    me.current = sound.id;
                    audio.src = sound.stream_url;
                    audio.play();
                };

                var getCurrent = function() {
                    var current = $scope.sounds.filter(function(sound) {
                        return sound.id === me.current;
                    }).pop();

                    return current;
                };

                this.prev = function() {
                    me.pause();

                    var index = $scope.sounds.indexOf(getCurrent());

                    me.play($scope.sounds[index - 1]);
                };

                this.next = function() {
                    me.pause();

                    var index = $scope.sounds.indexOf(getCurrent());

                    me.play($scope.sounds[index + 1]);
                };

                this.resume = function() {
                    me.isPaused = false;
                    audio.play();
                };

                this.pause = function() {
                    me.isPaused = true;
                    audio.pause();
                };

                this.isPlaying = function(id) {
                    return me.current === id && !me.isPaused;
                };

                this.loadMore = function() {
                    SoundsService.loadMore().success(function(next) {
                        $scope.sounds = $scope.sounds.concat(next.tracks);
                        SoundsService.nextUrl = next.next_href;
                        setScrolling();
                    });
                };

                this.isCurrent = function(id) {
                    return me.current === id;
                };
            },

            controllerAs: 'soundsCtrl'
        };
    });

})();