<?

$POST = json_decode(file_get_contents('php://input'), true);
$url = $POST['url'];

$tuCurl = curl_init();

curl_setopt($tuCurl, CURLOPT_URL, $url);
curl_setopt($tuCurl, CURLOPT_HEADER, 0); 

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");
header("Content-Type: application/json");

$tuData = curl_exec($tuCurl);

?>